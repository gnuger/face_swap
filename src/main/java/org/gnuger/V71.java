package org.gnuger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.face.tracking.clm.CLMFaceTracker;
import org.openimaj.image.processing.face.tracking.clm.MultiTracker.TrackedFace;
import org.openimaj.image.processing.transform.PiecewiseMeshWarp;
import org.openimaj.math.geometry.shape.Rectangle;
import org.openimaj.math.geometry.shape.Shape;
import org.openimaj.math.geometry.shape.Triangle;
import org.openimaj.math.geometry.transforms.TransformUtilities;
import org.openimaj.util.pair.Pair;

import Jama.Matrix;

@WebServlet("/v71")
@MultipartConfig
public class V71 extends HttpServlet {

	private static final long serialVersionUID = -5821947554254648501L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.getRequestDispatcher("/form_v71.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		MBFImage sImage = ImageUtilities.readMBF(req.getPart("source")
				.getInputStream());
		MBFImage dImage = ImageUtilities.readMBF(new URL(req.getParameter("destination")));

		CLMFaceTracker sTracker = new CLMFaceTracker();
		CLMFaceTracker dTracker = new CLMFaceTracker();
		TrackedFace sFace = findFace(sImage, sTracker);
		if (sFace == null) {
			sImage = rotateLeft(sImage);
			sFace = findFace(sImage, sTracker);
			if (sFace == null) {
				sImage = sImage.flipX().flipY();
				sFace = findFace(sImage, sTracker);
			}
		}
		TrackedFace dFace = findFace(dImage, dTracker);
		if (dFace == null) {
			dImage = rotateLeft(dImage);
			dFace = findFace(dImage, dTracker);
			if (dFace == null) {
				dImage = dImage.flipX().flipY();
				dFace = findFace(dImage, dTracker);
			}
		}

		if (sFace != null && dFace != null) {
			List<Triangle> sTriangles = sTracker.getTriangles(sFace);
			List<Triangle> dTriangles = dTracker.getTriangles(dFace);

			sImage.processInplace(computeMatches(sTriangles, dTriangles));
			composite(dImage, sImage, dFace.redetectedBounds.clone());

			resp.setContentType("image/jpeg");
			ImageUtilities.write(dImage, "jpg", resp.getOutputStream());
		} else if (sFace == null) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Source face not found");
		} else {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Destination face not found");
		}
	}

	private TrackedFace findFace(MBFImage image, CLMFaceTracker tracker) {
		TrackedFace finalFace = null;
		float confidence = 0;
		tracker.track(image);
		for (TrackedFace face : tracker.getTrackedFaces()) {
			if (face.getConfidence() > confidence) {
				finalFace = face;
				confidence = face.getConfidence();
			}
		}
		return finalFace;
	}

	private MBFImage rotateLeft(MBFImage image) {
		int height = image.getHeight();
		int width = image.getWidth();
		MBFImage result = image.clone();
		if (height > width) {
			result = result.paddingSymmetric(height - width, 0, 0, 0);
		} else if (width > height) {
			result = result.paddingSymmetric(0, 0, 0, width - height);
		}
		Matrix rotMatrix = TransformUtilities.centeredRotationMatrix(
				-Math.PI / 2, result.getCols(), result.getRows());
		result = result.transform(rotMatrix);

		if (height != width) {
			result = result.extractROI(0, 0, height, width);
		}

		return result;
	}

	private void composite(MBFImage back, MBFImage fore, Rectangle bounds) {
		bounds.height += 20;
		bounds.width += 20;
		bounds.x -= 10;
		bounds.y -= 10;

		final float[][] rin = fore.bands.get(0).pixels;
		final float[][] gin = fore.bands.get(1).pixels;
		final float[][] bin = fore.bands.get(2).pixels;

		final float[][] rout = back.bands.get(0).pixels;
		final float[][] gout = back.bands.get(1).pixels;
		final float[][] bout = back.bands.get(2).pixels;

		final int xmin = (int) Math.max(0, bounds.x);
		final int ymin = (int) Math.max(0, bounds.y);

		final int ymax = (int) Math.min(
				Math.min(fore.getHeight(), back.getHeight()), bounds.y
						+ bounds.height);
		final int xmax = (int) Math.min(
				Math.min(fore.getWidth(), back.getWidth()), bounds.x
						+ bounds.width);

		for (int y = ymin; y < ymax; y++) {
			for (int x = xmin; x < xmax; x++) {
				if (rin[y][x] != 0 && gin[y][x] != 0 && bin[y][x] != 0) {
					rout[y][x] = rin[y][x];
					gout[y][x] = gin[y][x];
					bout[y][x] = bin[y][x];
				}
			}
		}
	}

	private PiecewiseMeshWarp<Float[], MBFImage> computeMatches(
			List<Triangle> from, List<Triangle> to) {
		final List<Pair<Shape>> matchingRegions = new ArrayList<Pair<Shape>>();

		for (int i = 0; i < from.size(); i++) {
			Triangle t1 = from.get(i);
			Triangle t2 = to.get(i);

			if (t1 != null && t2 != null) {
				matchingRegions.add(new Pair<Shape>(t1, t2));
			}
		}

		return new PiecewiseMeshWarp<Float[], MBFImage>(matchingRegions);
	}

}
